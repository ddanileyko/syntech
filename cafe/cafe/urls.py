from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from apps.tables.urls import tables_urlpatterns

urlpatterns = []
urlpatterns += tables_urlpatterns
urlpatterns += [
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
