function show_hall() {
    var reserve_date = $('#reserve-date').val();
    url = window.location.protocol + '//' + window.location.host + '/?reserve_date=' + reserve_date;
    window.location = url;
};

$(document).ready(function() {

    $('#reserve-date').flatpickr({ minDate: "today", dateFormat: "Y-m-d" });

    $('.table-action').on('click', function() {
        var table_id = $(this).data('table');
        var own_reserve = $(this).data('own_reserve');
        var reserve_date = $('#reserve-date').val();
        do_post = 1;
        if (own_reserve == 'True') {do_post = 0; if (confirm('Удалить?')) {do_post = 1}};
        if (do_post == 1) {
        $.ajax({
            url: '/reserve/' + table_id + '/',
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({'reserve_date': reserve_date}),
            success: function(data) {
                if(data.success) {
                    show_hall();
                } else {
                    $('#cafe-messages').html(
                        '<div class="alert alert-danger">' +
                        '<button type="button" class="close" data-dismiss="alert">×</button>' +
                        data.message +
                        '</div>'
                    );
                }
            }
        });
        };
    });

    $('#reserve-date').on('change', function() { show_hall() });

});