from django.contrib import admin

from .models import Table


class TableAdmin(admin.ModelAdmin):
    list_display = ('number', 'shape', 'width', 'length', 'x', 'y',)


admin.site.register(Table, TableAdmin)
