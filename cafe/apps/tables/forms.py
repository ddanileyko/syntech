from django import forms
from django.utils.translation import ugettext_lazy as _


class Reservation(forms.Form):
    email = forms.EmailField(label=_('Email'))
    name = forms.CharField(label=_('Name'), max_length=30)