import datetime
import json
import smtplib
from django.contrib.auth import login
from jsonview.decorators import json_view
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.conf import settings

from .models import Table, Order, Cart, CartItem
from apps.users.models import User
from .forms import Reservation


def reservation(request):

    if request.method == 'POST':
        reservation_form = Reservation(request.POST)
        if reservation_form.is_valid():
            email = reservation_form.cleaned_data.get('email')
            name = reservation_form.cleaned_data.get('name')
            user, created = User.objects.get_or_create(email=email)
            user.name = name
            user.save()
            login(request, user)
            in_cart = CartItem.objects.filter(cart__session_key=request.session.session_key)
            email_mess = ''
            email_mess_err = ''
            for cart_item in in_cart:
                order, created = Order.objects.get_or_create(user=user, table=cart_item.table,
                                                             order_date=cart_item.order_date)
                if created:
                    email_mess += str(cart_item.table.number) + _(' by date ') + str(cart_item.order_date) + '; '
                    order = Order(user=user, table=cart_item.table, order_date=cart_item.order_date)
                    order.save()
                else:
                    email_mess_err += str(cart_item.table.number) + _(' by date ') + str(cart_item.order_date) + '; '
                cart_item.delete()
            if email_mess:
                email_mess = _('Your have reserved such tables: ') + email_mess
                messages.add_message(request, messages.SUCCESS, email_mess)
            if email_mess_err:
                email_mess_err = _('Sorry, a table has already been booked: ') + email_mess_err
                messages.add_message(request, messages.WARNING, email_mess_err)

            gmail_user = settings.EMAIL
            gmail_password = settings.EMAIL_PASSWORD
            TO = [email]
            SUBJECT = _('Reservation')
            BODY = "\r\n".join((
                "From: %s" % gmail_user,
                "To: %s" % TO,
                "Subject: %s" % SUBJECT,
                "",
                email_mess + '\n' + email_mess_err
            ))
            try:
                server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
                server.ehlo()
                server.login(gmail_user, gmail_password)
                server.sendmail('test@cafe.com', [email], BODY)
                server.close()
            except:
                messages.add_message(request, messages.ERROR, _('Mail not sent.'))

            cart = Cart.objects.filter(session_key=request.session.session_key).first()
            if cart:
                cart.delete()

    reserve_date = request.GET.get('reserve_date', str(datetime.datetime.now().date()))

    tables = Table.objects.all()
    reserved = Order.objects.filter(order_date=reserve_date).select_related()
    in_cart = CartItem.objects.filter(cart__session_key=request.session.session_key, order_date=reserve_date)

    reserved_tables = set()
    own_reserved_tables = set()
    tables_in_cart = set()

    for order in reserved:
        if order.user == request.user:
            own_reserved_tables.add(order.table.id)
        else:
            reserved_tables.add(order.table.id)

    for cart_item in in_cart:
        tables_in_cart.add(cart_item.table.id)

    for table in tables:
        table.color = Table.FREE_COLOR
        table.status = _('Free')
        if table.id in reserved_tables:
            table.color = Table.RESERVED_COLOR
            table.status = _('Reserved')
        own_reserve = False
        if table.id in own_reserved_tables:
            own_reserve = True
            table.color = Table.OWN_RESERVED_COLOR
            table.status = _('Your reserve')
        table.own_reserve = own_reserve
        if table.id in tables_in_cart:
            table.color = Table.CART_COLOR
            table.status = _('In cart')
        table.radius = 50 if table.shape == 2 else 0

    in_cart = CartItem.objects.filter(cart__session_key=request.session.session_key)

    if request.user.id:
        reserve_form = Reservation(initial={'email': request.user.email, 'name': request.user.name})
    else:
        reserve_form = Reservation()

    template = 'tables/reserve.html'

    context = {'tables': tables, 'reserve_date': reserve_date, 'in_cart': in_cart,
               'reserve_form': reserve_form, 'own_reserve': own_reserve}

    return render(request, template, context)


@json_view
@csrf_exempt
def reserve_table(request, table_id):
    if request.method == 'POST':
        table = Table.objects.filter(pk=table_id).first()
        if not table:
            return {'success': False, 'message': _('Something went wrong.')}

        order_date = json.loads(request.body.decode('utf-8')).get('reserve_date', None)

        if request.user.id:
            order = Order.objects.filter(table=table, order_date=order_date, user=request.user).first()
            if order:
                order.delete()
                return {'success': True, 'message': _('You reservation has bin canceled.')}

        cart_item = CartItem.objects.filter(cart__session_key=request.session.session_key,
                                            table=table, order_date=order_date).first()
        if cart_item:
            cart = cart_item.cart
            cart_item.delete()
            if CartItem.objects.filter(cart=cart).count() == 0:
                cart.delete()
            return {'success': True, 'message': _('You order hes been removed from the cart.')}

        cart, created = Cart.objects.get_or_create(session_key=request.session.session_key)
        cart.session = request.session.session_key
        cart.save()
        cart_item = CartItem(cart=cart, order_date=order_date, table=table)
        cart_item.save()

        return {'success': True, 'message': _('You have chosen a reservation table.')}
