# Generated by Django 2.2.12 on 2020-04-02 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tables', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='table',
            name='seats',
            field=models.PositiveSmallIntegerField(default=4, verbose_name='number of seats'),
            preserve_default=False,
        ),
    ]
