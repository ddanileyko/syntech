from django.urls import path

from .views import *

tables_urlpatterns = [
    path('', reservation, name='reservation'),
    path('reserve/<int:table_id>/', reserve_table, name='reserve_table'),
]
