from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


class Table(models.Model):
    RECTANGLE = 1
    OVAL = 2

    SHAPE = (
        (RECTANGLE, _('Rectangle')),
        (OVAL, _('Oval')),
    )

    FREE_COLOR = 'LawnGreen'
    CART_COLOR = 'Coral'
    RESERVED_COLOR = 'LawnGreen'
    OWN_RESERVED_COLOR = 'Tomato'

    number = models.PositiveSmallIntegerField(_('number'), unique=True)
    shape = models.PositiveSmallIntegerField(_('shape'), choices=SHAPE, default=RECTANGLE)
    seats = models.PositiveSmallIntegerField(_('number of seats'))
    width = models.PositiveSmallIntegerField(_('width'))
    length = models.PositiveSmallIntegerField(_('length'))
    x = models.PositiveSmallIntegerField(_('coordinate x'))
    y = models.PositiveSmallIntegerField(_('coordinate y'))

    class Meta:
        verbose_name = _('Table')
        verbose_name_plural = _('Table')

    def __str__(self):
        return str(self.number)


class Cart(models.Model):
    session_key = models.CharField(_('name'), max_length=32)
    added = models.DateTimeField(_('added'), auto_now_add=True)

    class Meta:
        ordering = ('added',)
        verbose_name = _('Cart')
        verbose_name_plural = _('Cart')

    def __str__(self):
        return u'%s' % self.session_key


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    order_date = models.DateField(_('order date'))
    table = models.ForeignKey(Table, on_delete=models.CASCADE)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    order_date = models.DateField(_('order date'))
    added = models.DateTimeField(_('added'), db_index=True, auto_now_add=True)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Order')

    def __str__(self):
        return u'%s -> %s' % (self.user.email, str(self.table.number))